function initAnimation() {
	const elementMavic = document.querySelector('.mavic_animation-js');

	elementMavic.addEventListener('mouseover', () => {
		elementMavic.classList.remove('no-hover');
		elementMavic.classList.add('hover');
	});

	elementMavic.addEventListener('mouseout', () => {
		elementMavic.classList.remove('hover');
		elementMavic.classList.add('no-hover');

	});
}

export { initAnimation };
